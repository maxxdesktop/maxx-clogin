# MaXX  CLogin

MaXX Desktop's Display Manager Implementation based on SGI's Clogin found on IRIX system.

- The general idea is to build a Display Manager that will look and behave like the one found on older SGI systems.  
- We could base our development fom XDM and maybe look at what the CDE project did.

The main goal is that Clogin is lean, fast and requires very little resources while keeping its dependencies low.